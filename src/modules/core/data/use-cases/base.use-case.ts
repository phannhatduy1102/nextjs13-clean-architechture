import { BaseRepository } from "../../domain";
import { makeBasePokemonRepository } from "../repositories";

export abstract class BaseUseCase {}

class BaseUseCaseImplement extends BaseUseCase {
  constructor(private baseRepository: BaseRepository) {
    super();
    this.baseRepository = baseRepository;
  }
}

export const makeBaseUseCase = () =>
  new BaseUseCaseImplement(makeBasePokemonRepository());

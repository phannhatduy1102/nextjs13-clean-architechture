import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { BaseRepository, POKEMON_API_URL, USER_API_URL } from "../../domain";
import { errorHandler } from "../../presenters";

const pokemonInstance: AxiosInstance = axios.create({
  timeout: 30000,
  baseURL: POKEMON_API_URL,
});

const userInstance: AxiosInstance = axios.create({
  timeout: 30000,
  baseURL: USER_API_URL,
});

class BaseRepositoryImplement implements BaseRepository {
  protected api: AxiosInstance;

  constructor(instance: AxiosInstance, enableErrorHandler: boolean) {
    this.api = instance;

    if (enableErrorHandler) {
      const methodNames = Object.getOwnPropertyNames(
        BaseRepositoryImplement.prototype
      ).filter((name) => name !== "constructor");

      for (const methodName of methodNames) {
        const descriptor = Object.getOwnPropertyDescriptor(
          BaseRepositoryImplement.prototype,
          methodName
        );
        if (descriptor?.value && typeof descriptor.value === "function") {
          Object.defineProperty(
            this,
            methodName,
            errorHandler(this, methodName, descriptor)
          );
        }
      }
    }
  }

  get<T>(url: string, config?: AxiosRequestConfig) {
    return this.api.get<T>(url, config);
  }

  post<T, D>(url: string, data?: D, config?: AxiosRequestConfig) {
    return this.api.post<T>(url, data, config);
  }

  put<T, D>(url: string, data?: D, config?: AxiosRequestConfig) {
    return this.api.put<T>(url, data, config);
  }

  delete<T>(url: string, config?: AxiosRequestConfig) {
    return this.api.delete<T>(url, config);
  }
}

export const makeBasePokemonRepository = (enableErrorHandler?: boolean) =>
  new BaseRepositoryImplement(pokemonInstance, enableErrorHandler || true);

export const makeBaseUserRepository = (enableErrorHandler?: boolean) =>
  new BaseRepositoryImplement(userInstance, enableErrorHandler || true);
